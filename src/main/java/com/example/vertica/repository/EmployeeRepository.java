package com.example.vertica.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.example.vertica.entity.Employee;
import com.example.vertica.exception.EmployeeNotFoundException;


@Repository
public class EmployeeRepository {
	 
	 @Autowired
	 JdbcTemplate jdbcTemplate;

	    public List<Employee> getAll() {
	     return jdbcTemplate.query("SELECT * from employee", BeanPropertyRowMapper.newInstance(Employee.class));
	    }

	    public int save(Employee employee) {
	        return jdbcTemplate.update("INSERT INTO employee (emp_name,department_name) VALUES(?,?)",
	                employee.getEmp_name(),employee.getDepartment_name());
	      }

	    public Employee findById(Long id) throws EmployeeNotFoundException{
	        try {
	            Employee employee = jdbcTemplate.queryForObject("SELECT * FROM employee WHERE emp_id=?",
	                    BeanPropertyRowMapper.newInstance(Employee.class), id);
	            return employee;
	        } catch (IncorrectResultSizeDataAccessException e) {
	        	  throw new EmployeeNotFoundException("Employee not found with id : "+id);
	        }
	    }

	    public int deleteById(Long id) throws EmployeeNotFoundException {
	    	Employee yy = this.findById(id);
	        return jdbcTemplate.update("DELETE FROM employee WHERE emp_id=?", id);
	    }

	    public int update(Long id ,Employee employee) throws EmployeeNotFoundException {
	    	Employee yy = this.findById(id);
	        return jdbcTemplate.update("UPDATE employee SET emp_name=?,department_name=? WHERE emp_id=?",
	                new Object[] { employee.getEmp_name(),employee.getDepartment_name(), id });
	    }
}
