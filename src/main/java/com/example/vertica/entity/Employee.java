package com.example.vertica.entity;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;



public class Employee implements Serializable{
	
	private static final long serialVersionUID = 6103498808951085533L;
	private Long emp_id;
	
	@NotBlank(message="Please provide some value to department as it cannot be blank")
	private String emp_name;
	@NotBlank(message="Please provide some value to department as it cannot be blank")
	private String department_name;
	
	public Employee(Long emp_id, String emp_name, String department_name) {
		super();
		this.emp_id = emp_id;
		this.emp_name = emp_name;
		this.department_name = department_name;
	}
	public Employee(String emp_name, String department_name) {
		super();
		this.emp_name = emp_name;
		this.department_name = department_name;
	}
	public Employee() {
		super();
	}
	public Long getEmp_id() {
		return emp_id;
	}
	public void setEmp_id(Long emp_id) {
		this.emp_id = emp_id;
	}
	public String getEmp_name() {
		return emp_name;
	}
	public void setEmp_name(String emp_name) {
		this.emp_name = emp_name;
	}
	public String getDepartment_name() {
		return department_name;
	}
	public void setDepartment_name(String department_name) {
		this.department_name = department_name;
	}
	@Override
	public String toString() {
		return "Employee [emp_id=" + emp_id + ", emp_name=" + emp_name + ", department_name=" + department_name
				+ ", getEmp_id()=" + getEmp_id() + ", getEmp_name()=" + getEmp_name() + ", getDepartment_name()="
				+ getDepartment_name() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + "]";
	}
	
	
}
