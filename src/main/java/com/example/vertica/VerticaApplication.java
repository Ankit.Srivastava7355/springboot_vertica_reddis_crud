package com.example.vertica;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableCaching
public class VerticaApplication {

	public static void main(String[] args) {
		SpringApplication.run(VerticaApplication.class, args);
	}

}
