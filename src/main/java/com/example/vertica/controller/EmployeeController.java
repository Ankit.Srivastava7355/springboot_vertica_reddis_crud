package com.example.vertica.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.vertica.entity.Employee;
import com.example.vertica.repository.EmployeeRepository;
import com.example.vertica.request.DepartmentRequest;
import com.example.vertica.response.ResponseHandler;
import com.example.vertica.serviceImpl.EmployeeServiceImpl;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1")
public class EmployeeController {
	
	@Autowired
	private EmployeeServiceImpl employeeserviceimpl;
	
	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(EmployeeController.class);
	
	// Get all the employees present in the database
	@GetMapping("/employees/getting")
	public ResponseEntity<Object> GetAllEmployees() {
		log.info("inside the get employee controller!!! ");
		 try {
	        	return ResponseHandler.generateResponse("All Employees Data fetched Successfully!", HttpStatus.OK, employeeserviceimpl.GetAllEmployees());
			}
			catch(Exception e) {
				return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, null);	
			}
		
	}
	
	// create an employee
	@PostMapping("/employees/adding")
	public ResponseEntity<Object> CreateAnEmployee(@Valid @RequestBody Employee employee) {
		log.info("inside the create an employee controller!!! ");
		 try {
	        	return ResponseHandler.generateResponse("Employee created Successfully!", HttpStatus.OK, employeeserviceimpl.CreateAnEmployee(employee));
			}
			catch(Exception e) {
				return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, null);	
			}	
	}
	
	// get a particular employee with given id
	@GetMapping("employees/{id}")
	public ResponseEntity<Object> ReadAnEmployee(@PathVariable("id") Long id) {
		log.info("inside the get a particular employee with given id controller!!! ");
		 try {
	        	return ResponseHandler.generateResponse("Employee with given id fetched Successfully!", HttpStatus.OK, employeeserviceimpl.GetAnEmployee(id));
			}
			catch(Exception e) {
				return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, null);	
			}			
	}
	
	// updating an employee with given id
	@PutMapping("employees/updating/{id}")
	public ResponseEntity<Object> UpdateAnEmployee(@PathVariable("id") Long id,@Valid @RequestBody Employee employee)
	{
		log.info("inside the employee updating controller!!! ");
		 try {
	        	return ResponseHandler.generateResponse("Employee data updated Successfully!", HttpStatus.OK, employeeserviceimpl.UpdateAnEmployee(id, employee));
			}
			catch(Exception e) {
				return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, null);	
			}	
		
	}

	// delete an employee with given id
	@DeleteMapping("employees/deleting/{id}")
	public ResponseEntity<Object> DeletingAnEmployee(@PathVariable("id") Long id) {
		log.info("inside the delete employee with given id controller!!! ");
		try {
			employeeserviceimpl.DeleteAnEmployee(id);
        	return ResponseHandler.generateResponse("Employee with given id deleted successfully!", HttpStatus.OK, "Employee deleted");
		}
		catch(Exception e) {
			return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, null);	
		}	
 	}
	
	// get the department of the employee with the given id and name of employee.
		@PostMapping("/employees/department")
		public ResponseEntity<Object> Department(@Valid @RequestBody DepartmentRequest department_details) {
			log.info("inside the get employee's department controller !!! ");
			try {
			    return ResponseHandler.generateResponse("Department of given employee fetched Successfully!", HttpStatus.OK, employeeserviceimpl.GetDepartmentOfEmployee(department_details.getEmp_id(), department_details.getEmp_name()).getDepartment_name());
	        } catch (Exception e) {
	            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, null);
	        }
		}
}