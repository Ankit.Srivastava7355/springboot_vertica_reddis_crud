package com.example.vertica.request;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class DepartmentRequest implements Serializable{
	
	private static final long serialVersionUID = 2785457602091759682L;
	
	@NotNull(message = "Employee ID is required")
    private Long emp_id;
    
    @NotNull(message = "Employee name is required")
    @Size(min = 1, max = 50, message = "Employee name must be between 1 and 50 characters long")
    private String emp_name;
    
	public DepartmentRequest(Long emp_id, String emp_name) {
		super();
		this.emp_id = emp_id;
		this.emp_name = emp_name;
	}
	public DepartmentRequest() {
		super();
	}
	public Long getEmp_id() {
		return emp_id;
	}
	public void setEmp_id(Long emp_id) {
		this.emp_id = emp_id;
	}
	public String getEmp_name() {
		return emp_name;
	}
	public void setEmp_name(String emp_name) {
		this.emp_name = emp_name;
	}
	@Override
	public String toString() {
		return "DepartmentRequest [emp_id=" + emp_id + ", emp_name=" + emp_name + ", getEmp_id()=" + getEmp_id()
				+ ", getEmp_name()=" + getEmp_name() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}
	
}
