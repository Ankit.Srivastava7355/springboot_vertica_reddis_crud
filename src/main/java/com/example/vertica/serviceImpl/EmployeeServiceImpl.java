package com.example.vertica.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.example.vertica.controller.EmployeeController;
import com.example.vertica.entity.Employee;
import com.example.vertica.exception.EmployeeNotFoundException;
import com.example.vertica.repository.EmployeeRepository;
import com.example.vertica.service.EmployeeService;



@Service
public class EmployeeServiceImpl implements EmployeeService{

	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(EmployeeServiceImpl.class);
	
	@Autowired
	private EmployeeRepository employeerepository;
	
	@Override
	public Employee CreateAnEmployee(Employee employee) {
		employeerepository.save(employee);
		return employee;
	}

	@Override
	@CachePut(value="employee",key="#id")
	public Employee UpdateAnEmployee(Long id, Employee employee) throws EmployeeNotFoundException  {
		log.info("updating employee in the database and cache as well");
		int gg = employeerepository.update(id, employee);
		Employee tt = employeerepository.findById(id);
		return tt;
	}

	@Override
	@Cacheable(value="employee",key="#id")
	public Employee GetAnEmployee(Long id) throws EmployeeNotFoundException {
		log.info("Employee not found in cache going now going for database");
		Employee gg = employeerepository.findById(id);
		return gg;
	}

	@Override
	@CacheEvict(value="employee",key="#id")
	public void DeleteAnEmployee(Long id) throws EmployeeNotFoundException {
		log.info("deleting employee from database first and then cache as well");
		Employee tt = employeerepository.findById(id);
		employeerepository.deleteById(id);
	}

	@Override
	@Cacheable(value="employee",key="#emp_id")
	public Employee GetDepartmentOfEmployee(Long emp_id, String emp_name) throws EmployeeNotFoundException {
		log.info("employee not found in cache now going inside database");
		Employee tt = employeerepository.findById(emp_id);
		return tt;
	}

	@Override
	public List<Employee> GetAllEmployees() {
		return employeerepository.getAll();
	}

	

}