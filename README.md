# SpringBoot_Vertica_Reddis_Cache_REST_APIs

## 2nd assignment :

API should perform CRUD using spring boot , vertica , redis

employee table can be used for CRUD operation .

used redis for employee & department mapping .

redis key < emp_id , department_name >

API input ( emp_id , emp_name )

emp_id should be check in redis to get the department_name .

If any employee id mapping not exists in redis then assign default department_name as ( default_dept )

department wise vertica schema should be precreated with employee table .

single database should be created in vertica name as smartech .

employee table schema employee(id int, name string, department_name string )

department datasource object should be created once in entire lifetime .

department datasource object should be cached using spring memcache .

if another employee with same DB name comes via api we should refer the mem cache data source and not create db data source again.

for CRUD operation we can connect to db .

API should have validations on strict types and compulsory parameters and it should return proper error codes & error messages .


# REST API ENDPOINTS : 
```
Get all the employees from the database -> GET => http://localhost:8082/api/v1/employees/getting
Create a new employee -> POST => http://localhost:8082/api/v1/employees/adding
Get a particular employee from the database -> GET => http://localhost:8082/api/v1/employees/{id}
Update a particular employee with the given id -> PUT => http://localhost:8082/api/v1/employees/updating/{id}
Delete a particular employee with the given id -> DELETE => http://localhost:8082/api/v1/employees/deleting/{id}
To fetch the department of an employee whose employee_id and employee_name is given -> POST => http://localhost:8082/api/v1/employees/department
```